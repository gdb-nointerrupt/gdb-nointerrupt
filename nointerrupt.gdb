define nointerrupt
	python

import gdb

PR_SET_NAME = 15

def switch_to_thread(tid, interrupt = False):
	gdb.execute('thr %d' % (tid,), from_tty=True)
	if interrupt:
		gdb.execute('interrupt')

def switch_to_gdb_thread():
	for thr in gdb.selected_inferior().threads():
		if thr.name == '__dbg':
			print("*** Using existing debug thread %d" % (thr.num,))
			return switch_to_thread(thr.num, True)
	
	steps = []
	tid = [None]
	def do_step(event):
		if steps: steps.pop(0)()
	
	def step1():
		gdb.execute('interrupt')
	
	def step2():
		gdb.execute('set $tmp_thr = malloc(sizeof(pthread_t))')
		gdb.parse_and_eval('pthread_create($tmp_thr, 0, pause, 0)')
		gdb.parse_and_eval('free($tmp_thr)')
		gdb.execute('continue &')
		
		tid[0] = len(gdb.selected_inferior().threads())
		print("*** Created new debug thread %d" % (tid[0],))
		switch_to_thread(tid[0], True)
	
	def step3():
		gdb.parse_and_eval('prctl(%d, "__dbg", 0, 0, 0)' % (PR_SET_NAME,))
		gdb.events.stop.disconnect(do_step)
	
	steps[:] = (step1, step2, step3)
	gdb.events.stop.connect(do_step)
	do_step(None)

switch_to_gdb_thread()

	end
end

define async-attach
	set target-async 1
	set pagination off
	set non-stop on
	attach $arg0
	continue -a &
	nointerrupt
	python print("Running, use 'nointerrupt' to access process without stopping")
end
